package com.emirates.transitadventures;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class AtmActivity  extends AppCompatActivity {
    ListView mAtmListView;
    AtmAdapter mAtmAdapter;
    List<AtmModel> mAtmList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atm_list);

        initialise();
    }

    /**
     * Bind, create and set up the resources
     */
    private void initialise() {
        // Toolbar
        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("ATM Locator");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);

        mAtmListView = (ListView) findViewById(R.id.atm_listview);
        mAtmList = new ArrayList<>();
        mAtmAdapter = new AtmAdapter(this, mAtmList);
        mAtmListView.setAdapter(mAtmAdapter);

        mAtmListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gotoNextScreen();
            }
        });

        //load atm list here
        AtmModel atmModel = new AtmModel();
        atmModel.branch = "FGB - Internet City";
        atmModel.location = "Building No 3, Dubai Internet City";
        atmModel.bank_type = AtmModel.FIRST_GULF_BANK;
        atmModel.lastUsed = "09:00 pm 20/10/2016";
        mAtmList.add(atmModel);

        atmModel = new AtmModel();
        atmModel.branch = "Emirates NBD - Media City";
        atmModel.location = "Building No 4, Dubai Media City";
        atmModel.bank_type = AtmModel.EMIRATES_NBD;
        atmModel.lastUsed = "12:00 pm 20/12/2016";
        mAtmList.add(atmModel);

        atmModel = new AtmModel();
        atmModel.branch = "Standard Chartered";
        atmModel.location = "Burj Khalifa, Downtown Dubai";
        atmModel.bank_type = AtmModel.STANDARD_CHARTER;
        atmModel.lastUsed = "09:00 pm 21/12/2016";
        mAtmList.add(atmModel);

        mAtmAdapter.notifyDataSetChanged();
    }

    private void gotoNextScreen(){
        final Intent intent = new Intent(this, SelectCardActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
    }

}

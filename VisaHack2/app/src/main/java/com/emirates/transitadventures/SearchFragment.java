package com.emirates.transitadventures;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;

public class SearchFragment extends Fragment implements View.OnClickListener {

    final CharSequence[] airportCodes = {"Dubai(DXB)", "Hyderabad(HYD)", "Jeddah(JED)",
            "Kochi(cok)", "Trinvandram(TRV)", "Calicut(CCJ)", "Bombay(BOM)", "Delhi(DEL)",
            "San Francisco(SFO)", "calcutta(CCU)", "Sydney(SYD)", "London(LHR)"};
    final CharSequence[] classTypeArry = {"First", "Business", "Economy"};
    private TextView originTv, destinationTv, calTv, classTv;
    private CalendarView calendar;
    private boolean isCalShowed;
    private static final String EMPTY_STRING = "";
    private static final String CLASS_TYPE_STRING = "Class : ";
    private Button searchBtn;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_activity, null);

        originTv = (TextView) view.findViewById(R.id.tv_origin);
        destinationTv = (TextView) view.findViewById(R.id.tv_destination);
        calTv = (TextView) view.findViewById(R.id.tv_origin_calendar);
        classTv = (TextView) view.findViewById(R.id.tv_class_type);
        classTv.setOnClickListener(this);
        calTv.setOnClickListener(this);
        originTv.setOnClickListener(this);
        destinationTv.setOnClickListener(this);
        isCalShowed = false;

        searchBtn = (Button) view.findViewById(R.id.login_button_login);
        searchBtn.setOnClickListener(this);
        calendar = (CalendarView) view.findViewById(R.id.calendar);
        return view;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_origin) {
            airportCodeList(v, "Choose Origin", airportCodes, EMPTY_STRING);
        } else if (v.getId() == R.id.tv_destination) {
            airportCodeList(v, "Choose Destination", airportCodes, EMPTY_STRING);
        } else if (v.getId() == R.id.tv_origin_calendar) {
            if (isCalShowed) {
                calendar.setVisibility(View.VISIBLE);
                initializeCalendar();
                isCalShowed = false;
            } else {
                calendar.setVisibility(View.GONE);
                isCalShowed = true;
            }
        } else if (v.getId() == R.id.tv_class_type) {
            airportCodeList(v, "Choose Class", classTypeArry, CLASS_TYPE_STRING);
        } else if (v.getId() == R.id.login_button_login) {
            startActivity(new Intent(getActivity(), SearchResultActivity.class));
        }
    }

    private void airportCodeList(final View v, final String header, final CharSequence[] dataArry, final String preString) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(header);
        builder.setItems(dataArry, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int index) {
                ((TextView) (v)).setText(preString + dataArry[index]);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void initializeCalendar() {

        // sets whether to show the week number.
        calendar.setShowWeekNumber(false);

        // sets the first day of week according to Calendar.
        // here we set Monday as the first day of the Calendar
        calendar.setFirstDayOfWeek(2);

        //The background color for the selected week.
        calendar.setSelectedWeekBackgroundColor(getResources().getColor(R.color.light_blue_bg));

        //sets the color for the dates of an unfocused month.
        calendar.setUnfocusedMonthDateColor(getResources().getColor(R.color.transparent));

        //sets the color for the separator line between weeks.
        calendar.setWeekSeparatorLineColor(getResources().getColor(R.color.transparent));

        //sets the color for the vertical bar shown at the beginning and at the end of the selected date.
        calendar.setSelectedDateVerticalBar(R.color.red_btn_hover);

        //sets the listener to be notified upon selected date change.
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            //show the selected date as a toast
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int day) {
                calTv.setText("Departure Date : " + day + "/" + (month + 1) + "/" + year);
            }
        });
    }

    private boolean viewValidator() {
        if (originTv != null && originTv.getText().length() > 0 && destinationTv != null && destinationTv.getText().length() > 0 && calTv != null && calTv.getText().length() > 0)
            return true;
        else
            return false;
    }
}


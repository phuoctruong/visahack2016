package com.emirates.transitadventures;

/**
 * Created by S441634 on 28/08/2015.
 */
public class FlightModel {

    private String tripDestination;
    private String flightNo;
    private String airportOriginDestinationCodes;
    private String flightPerios;
    private String airCraftType;
    private String flightDuration;
    private String nextDay;
    private String connectionTerminal;
    private boolean isTransitPackAvailable;

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getAirportOriginDestinationCodes() {
        return airportOriginDestinationCodes;
    }

    public void setAirportOriginDestinationCodes(String airportOriginDestinationCodes) {
        this.airportOriginDestinationCodes = airportOriginDestinationCodes;
    }

    public String getFlightPerios() {
        return flightPerios;
    }

    public void setFlightPerios(String flightPerios) {
        this.flightPerios = flightPerios;
    }

    public String getAirCraftType() {
        return airCraftType;
    }

    public void setAirCraftType(String airCraftType) {
        this.airCraftType = airCraftType;
    }

    public String getFlightDuration() {
        return flightDuration;
    }

    public void setFlightDuration(String flightDuration) {
        this.flightDuration = flightDuration;
    }

    public String getNextDay() {
        return nextDay;
    }

    public void setNextDay(String nextDay) {
        this.nextDay = nextDay;
    }

    public String getConnectionTerminal() {
        return connectionTerminal;
    }

    public void setConnectionTerminal(String connectionTerminal) {
        this.connectionTerminal = connectionTerminal;
    }

    public boolean isTransitPackAvailable() {
        return isTransitPackAvailable;
    }

    public void setIsTransitPackAvailable(boolean isTransitPackAvailable) {
        this.isTransitPackAvailable = isTransitPackAvailable;
    }

    public String getTripOrigin() {
        return tripOrigin;
    }

    public void setTripOrigin(String tripOrigin) {
        this.tripOrigin = tripOrigin;
    }

    private String tripOrigin;

    public String getTripDestination() {
        return tripDestination;
    }

    public void setTripDestination(String tripDestination) {
        this.tripDestination = tripDestination;
    }


}

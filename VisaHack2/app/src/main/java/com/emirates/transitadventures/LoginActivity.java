package com.emirates.transitadventures;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by s728304 on 26/02/2016.
 */
public class LoginActivity extends AppCompatActivity {

    public static final String VAR_MEMBER_LOGIN = "MEMBER_LOGIN";

    private static final String TAG = LoginActivity.class.getSimpleName();
    private Handler handler;
    CallbackManager callbackManager;
    private EditText username;
    HashMap<String, String> mHashMap = new HashMap<String, String>();

    //    private EditText password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login_view);
        Button memberSignIn = (Button) findViewById(R.id.button);
        username = (EditText) findViewById(R.id.usernameId);
        username.setText("447700100050");
//        password =  (EditText)findViewById(R.id.passwordId);
        prepareMobileConnectSubData();
        memberSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onConfirmClick();
                if (username.getText() != null && username.getText().length() == 0) {
                    makeToast(getBaseContext(), "Please enter registered phone number");
                } else if (username.getText() != null && username.getText().length() > 0) {
                    if (isSubAvailable(username.getText().toString())) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                final Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                                intent.putExtra(VAR_MEMBER_LOGIN, true);
                                startActivity(intent);
                                overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
                                finish();
                            }
                        }, 1000);
                    } else {
                        makeToast(getBaseContext(), "Phone number not yet registered");
                    }
                }
            }
        });
        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
                        intent.putExtra(VAR_MEMBER_LOGIN, false);
                        startActivity(intent);
                        overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
                        finish();
                    }
                }, 1000);

            }

        });


        loginButton.setReadPermissions(Arrays.asList("user_likes", "user_status", "email", "user_birthday"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(TAG, "Facebook Login Success!");
                GraphRequest request = GraphRequest.newMeRequest
                        (loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                // Application code
                                Log.v(TAG, response.toString());
                                //System.out.println("Check: " + response.toString());
                                try {
                                    String id = object.getString("id");
                                    String name = object.getString("name");
                                    String email = object.getString("email");
                                    String gender = object.getString("gender");
                                    String birthday = object.getString("birthday");
                                    System.out.println(id + ", " + name + ", " + email + ", " + gender + ", " + birthday);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "Facebook Login Cancelled");
            }

            @Override
            public void onError(FacebookException e) {
                Log.e(TAG, "Facebook Login Exception: ", e);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void onConfirmClick() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, "Please Wait...");
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();

            }
        }, 1000);

    }


    private void prepareMobileConnectSubData() {
        mHashMap.put("447700100010", "61fcee18fe43b86ca06aae733a40079b");
        mHashMap.put("447700100020", "61fcee18fe43b86ca06aae733a40079b");
        mHashMap.put("447700100030", "61fcee18fe43b86ca06aae733a40079b");
        mHashMap.put("447700100040", "61fcee18fe43b86ca06aae733a40079b");
        mHashMap.put("447700100050", "61fcee18fe43b86ca06aae733a40079b");
        mHashMap.put("447700100060", "61fcee18fe43b86ca06aae733a40079b");
    }

    private boolean isSubAvailable(String phoneNo) {
        if (mHashMap.containsKey(phoneNo))
            return true;
        else
            return false;
    }

    static void makeToast(Context ctx, String s) {
        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
    }

}

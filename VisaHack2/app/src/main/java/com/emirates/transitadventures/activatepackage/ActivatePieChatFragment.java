package com.emirates.transitadventures.activatepackage;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.emirates.transitadventures.DescriptionActivity;
import com.emirates.transitadventures.R;
import com.emirates.transitadventures.model.RowItem;
import com.emirates.transitadventures.piechart.PieChart;

import java.util.ArrayList;

/**
 * Created by S442095 on 28/08/2015.
 */
public class ActivatePieChatFragment extends Fragment implements PieChart.OnSelectedLisenter, View.OnClickListener {

    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.activate_chart, null);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, r.getDisplayMetrics());
        width = width - (int) px;

        view = contentView.findViewById(R.id.pie_chart1);

        FrameLayout frameLayout = (FrameLayout) contentView.findViewById(R.id.pie_chart_frame1);

        ArrayList<RowItem> rowItems = createData1();
        final PieChart pieChart = new PieChart(getActivity(), width, rowItems);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width, width);
        frameLayout.addView(pieChart, layoutParams);
        pieChart.setOnSelectedListener(this);
        ArrayList<Float> percentage = new ArrayList<Float>();
        percentage.add(20F);
        percentage.add(20F);
        percentage.add(20F);
        percentage.add(20F);
        percentage.add(20F);
        try {
            pieChart.setAdapter(percentage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        View labelValue = contentView.findViewById(R.id.event1);

        TextView tv = (TextView) labelValue.findViewById(R.id.label);
        tv.setText(rowItems.get(0).getName());
        tv = (TextView) labelValue.findViewById(R.id.time);
        tv.setText(rowItems.get(0).getTimings());

        labelValue = contentView.findViewById(R.id.event2);
        tv = (TextView) labelValue.findViewById(R.id.label);
        tv.setText(rowItems.get(1).getName());
        tv = (TextView) labelValue.findViewById(R.id.time);
        tv.setText(rowItems.get(1).getTimings());

        labelValue = contentView.findViewById(R.id.event3);
        tv = (TextView) labelValue.findViewById(R.id.label);
        tv.setText(rowItems.get(2).getName());
        tv = (TextView) labelValue.findViewById(R.id.time);
        tv.setText(rowItems.get(2).getTimings());

        labelValue = contentView.findViewById(R.id.event4);
        tv = (TextView) labelValue.findViewById(R.id.label);
        tv.setText(rowItems.get(3).getName());
        tv = (TextView) labelValue.findViewById(R.id.time);
        tv.setText(rowItems.get(3).getTimings());

        labelValue = contentView.findViewById(R.id.event5);
        tv = (TextView) labelValue.findViewById(R.id.label);
        tv.setText(rowItems.get(4).getName());
        tv = (TextView) labelValue.findViewById(R.id.time);
        tv.setText(rowItems.get(4).getTimings());

        CheckBox checkBox = (CheckBox) view.findViewById(R.id.check_box_1);
        checkBox.setVisibility(View.GONE);
        showNotification();
        return contentView;
    }

    private void showNotification() {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getActivity())
                        .setSmallIcon(R.drawable.app_logo)
                        .setContentTitle("#Emirates Islamic")
                        .setContentText("Let it start.");

        NotificationManager mNotificationManager =
                (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(1, mBuilder.build());
    }

    @Override
    public void onClick(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        alertDialogBuilder.setTitle("Activate");
        alertDialogBuilder
                .setMessage("You now have complete control on your itinerary. Enjoy!")
                .setCancelable(false)
                .setPositiveButton("OK", null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private ArrayList<RowItem> createData1() {
        ArrayList<RowItem> rowItems = new ArrayList<>();
        addDash(rowItems);
        addDubaiMarina(rowItems);
        addMeusum(rowItems);
        addBurjGhalifa(rowItems);
        addPalm(rowItems);
        return rowItems;
    }

    private void addDash(ArrayList<RowItem> rowItems) {
        RowItem rowItem = new RowItem(R.drawable.act_1, "act_24");
        rowItem.setDetails("Explore this in dubai");
        rowItem.setName("DUNE BASHING");
        rowItem.setTimings("6:00PM-11PM");
        rowItem.setPrice("140AED");
        rowItems.add(rowItem);
    }

    private void addDubaiMarina(ArrayList<RowItem> rowItems) {
        RowItem rowItem = new RowItem(R.drawable.act_2, "dubai_marina");
        rowItem.setDetails("Get Inspired by the creative building");
        rowItem.setName("DUBAI MARINA");
        rowItem.setTimings("9:00AM-11AM");
        rowItem.setPrice("200AED");
        rowItems.add(rowItem);
    }

    private void addMeusum(ArrayList<RowItem> rowItems) {
        RowItem rowItem = new RowItem(R.drawable.act_3, "meuseum");
        rowItem.setDetails("Travel in history");
        rowItem.setName("MEUSEUM");
        rowItem.setTimings("11:00AM-1PM");
        rowItem.setPrice("100AED");
        rowItems.add(rowItem);
    }

    private void addBurjGhalifa(ArrayList<RowItem> rowItems) {
        RowItem rowItem = new RowItem(R.drawable.act_4, "b_khalifa");
        rowItem.setDetails("World's Larget Man Made Tower");
        rowItem.setName("BURJ KHALIFA");
        rowItem.setTimings("5:00PM-6PM");
        rowItem.setPrice("140AED");
        rowItems.add(rowItem);
    }

    private void addPalm(ArrayList<RowItem> rowItems) {
        RowItem rowItem = new RowItem(R.drawable.act_1, "palm");
        rowItem.setDetails("The Best Man Made Island Ever");
        rowItem.setName("PALM JUMERAH");
        rowItem.setTimings("2:00PM-5PM");
        rowItem.setPrice("180AED");
        rowItems.add(rowItem);
    }

    @Override
    public void onSelected(RowItem rowItem, PieChart obj) {
        final Intent intent = new Intent(getActivity(), DescriptionActivity.class);
        intent.putExtra("rowITem", rowItem);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.abc_slide_in_bottom, 0);

    }

}

package com.emirates.transitadventures;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class AtmAdapter extends ArrayAdapter<AtmModel> {

    public AtmAdapter(Context context, List<AtmModel> models) {
        super(context, R.layout.atm_item, models);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = View.inflate(getContext(), R.layout.atm_item, null);
            Holder holder = new Holder();
            holder.branch = (TextView) view.findViewById(R.id.atm_branch);
            holder.imageView = (ImageView) view.findViewById(R.id.atm_photo);
            holder.bankLogo = (ImageView) view.findViewById(R.id.bank_logo);
            holder.lastUsed = (TextView) view.findViewById(R.id.atm_last_used);
            view.setTag(holder);
        }
        Holder holder = (Holder) view.getTag();
        AtmModel atmModel = getItem(position);
        holder.branch.setText("Location: " + atmModel.location);
        holder.lastUsed.setText("Last used: " + atmModel.lastUsed);
        switch (atmModel.bank_type) {
            case AtmModel.EMIRATES_NBD:
                holder.imageView.setImageResource(R.drawable.atm_avi);
                holder.bankLogo.setImageResource(R.drawable.enbd);
                break;
            case AtmModel.STANDARD_CHARTER:
                holder.imageView.setImageResource(R.drawable.atm_avi);
                holder.bankLogo.setImageResource(R.drawable.sc);
                break;
            case AtmModel.FIRST_GULF_BANK:
                holder.imageView.setImageResource(R.drawable.atm_avi);
                holder.bankLogo.setImageResource(R.drawable.fgb);
                break;
        }
        return view;
    }

    private class Holder {
        private ImageView imageView;
        private ImageView bankLogo;
        private TextView branch;
        private TextView lastUsed;
    }
}

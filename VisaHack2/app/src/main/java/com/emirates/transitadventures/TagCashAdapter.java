package com.emirates.transitadventures;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class TagCashAdapter extends ArrayAdapter<String> {
    private static final String DELIMITER = "#";
    private static final String[] PREFIXES = new String[]{"Buying a", "with"};
    private List<String> items;
    private List<String> itemsAll;
    private List<String> suggestions;
    private List<List<String>> hashList;

    public TagCashAdapter(Context context, List<String> values) {
        super(context, R.layout.tag_cash_item, values);
        this.items = values;
        this.itemsAll = new ArrayList<>();
        this.itemsAll.addAll(items);
        this.suggestions = new ArrayList<String>();
        hashList = new ArrayList<>();
        hashList.add(new ArrayList<String>());
        hashList.add(new ArrayList<String>());
    }

    public void putTagData(int position, List<String> data){
        if(position <= hashList.size() - 1 ){
            List<String> list = (List<String>) hashList.get(position);
            list.clear();
            list.addAll(data);
        }
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null){
            view = View.inflate(getContext(), R.layout.tag_cash_item, null);
            Holder holder = new Holder();
            holder.content = (TextView) view.findViewById(R.id.content);
            view.setTag(holder);
        }
        Holder holder = (Holder) view.getTag();
        String atmModel = getItem(position);
        holder.content.setText(atmModel);
        return view;
    }

    private List<String> getTag (CharSequence constraint){
        int count = constraint.toString().length() - constraint.toString().replace(DELIMITER, "").length();
        if(count <= hashList.size()){
            return hashList.get(count - 1);
        }
        return new ArrayList<>();
    }

    private class Holder{
        public TextView content;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {

        private String getConstraint(CharSequence charSequence){
            int lastInx = charSequence.toString().lastIndexOf(DELIMITER);
            if(lastInx + 1 < charSequence.length()){
                return charSequence.toString().substring(lastInx + 1);
            }
            return "";
        }

        private String getPrefix (CharSequence charSequence){
            int lastInx = charSequence.toString().lastIndexOf(DELIMITER);
            return charSequence.toString().substring(0, lastInx) + DELIMITER;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null && constraint.toString().contains(DELIMITER)) {
                String realCs = getConstraint(constraint);
                itemsAll = getTag(constraint);
                String prefix = getPrefix(constraint);
                suggestions.clear();
                for (String product : itemsAll) {
                    if (product.toLowerCase()
                            .contains(realCs.toLowerCase())) {
                        suggestions.add(prefix + product);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            @SuppressWarnings("unchecked")
            ArrayList<String> filteredList = (ArrayList<String>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (String c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };


}

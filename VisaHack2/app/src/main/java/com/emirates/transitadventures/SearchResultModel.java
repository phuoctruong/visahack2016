package com.emirates.transitadventures;

import java.util.ArrayList;

/**
 * Created by S441634 on 28/08/2015.
 */
public class SearchResultModel {

    private String origin;
    private String destination;
    private String date;
    private String depTime;
    private String arrTime;
    private String noOfStops;
    private String currencyType;
    private String currency;
    private String noOfPax;
    private String durationHrs;
    private String durationMins;

    public ArrayList<FlightModel> getFlightList() {
        return flightList;
    }

    public void setFlightList(ArrayList<FlightModel> flightList) {
        this.flightList = flightList;
    }

    private ArrayList<FlightModel> flightList;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDepTime() {
        return depTime;
    }

    public void setDepTime(String depTime) {
        this.depTime = depTime;
    }

    public String getArrTime() {
        return arrTime;
    }

    public void setArrTime(String arrTime) {
        this.arrTime = arrTime;
    }

    public String getNoOfStops() {
        return noOfStops;
    }

    public void setNoOfStops(String noOfStops) {
        this.noOfStops = noOfStops;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getNoOfPax() {
        return noOfPax;
    }

    public void setNoOfPax(String noOfPax) {
        this.noOfPax = noOfPax;
    }

    public String getDurationHrs() {
        return durationHrs;
    }

    public void setDurationHrs(String durationHrs) {
        this.durationHrs = durationHrs;
    }

    public String getDurationMins() {
        return durationMins;
    }

    public void setDurationMins(String durationMins) {
        this.durationMins = durationMins;
    }

}

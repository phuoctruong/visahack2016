package com.emirates.transitadventures;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;

public class TagCashActivity extends AppCompatActivity {
    AutoCompleteTextView mTagCashTextView;
    TagCashAdapter mTagCashAdapter;
    List<String> mTagList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tag_cash);

        initialise();
    }

    /**
     * Bind, create and set up the resources
     */
    private void initialise() {
        // Toolbar
        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("Tag Cash");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);

        mTagCashTextView = (AutoCompleteTextView) findViewById(R.id.tag_cash_textview);
        mTagCashAdapter = new TagCashAdapter(this, new ArrayList<String>());
        mTagCashTextView.setAdapter(mTagCashAdapter);

        //init values
        mTagList = new ArrayList<>();
        mTagList.add("Big Mac 20 AED");
        mTagList.add("Whooper 23 AED");
        mTagList.add("Chicken 15 AED");
        mTagList.add("Salad 10 AED");
        mTagList.add("Pepsi 5 AED");
        mTagCashAdapter.putTagData(0, mTagList);

        ArrayList<String> cardList = new ArrayList<>();
        cardList.add("Card 1 - 4111");
        cardList.add("Card 2 - 5777");
        cardList.add("Others - 1234");
        mTagCashAdapter.putTagData(1, cardList);
    }

    private void gotoNextScreen() {
        final Intent intent = new Intent(this, SelectCardActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
    }

}

package com.emirates.transitadventures;


import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class SeasonsFragment extends Fragment implements View.OnClickListener {


    private CalendarView calendar;
    private boolean isCalShowed;
    private static final String EMPTY_STRING = "";
    private static final String CLASS_TYPE_STRING = "Class : ";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_home, null);


        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.anim_enter);
        Button summerBtn = (Button) view.findViewById(R.id.buton_summer);
        summerBtn.startAnimation(bottomUp);
        summerBtn.setVisibility(View.VISIBLE);

        Button winterBtn = (Button) view.findViewById(R.id.buton_winter);
        winterBtn.startAnimation(bottomUp);
        winterBtn.setVisibility(View.VISIBLE);

        Button rainyBtn = (Button) view.findViewById(R.id.buton_rainy);
        rainyBtn.startAnimation(bottomUp);
        rainyBtn.setVisibility(View.VISIBLE);

        Button springBtn = (Button) view.findViewById(R.id.buton_Spring);
        springBtn.startAnimation(bottomUp);
        springBtn.setVisibility(View.VISIBLE);

        isCalShowed = false;

//        searchBtn = (Button) view.findViewById(R.id.login_button_login);
//        searchBtn.setOnClickListener(this);
        calendar = (CalendarView) view.findViewById(R.id.calendar);
        calendar.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    public void initializeCalendar() {

        // sets whether to show the week number.
        calendar.setShowWeekNumber(false);

        // sets the first day of week according to Calendar.
        // here we set Monday as the first day of the Calendar
        calendar.setFirstDayOfWeek(2);

        //The background color for the selected week.
        calendar.setSelectedWeekBackgroundColor(getResources().getColor(R.color.light_blue_bg));

        //sets the color for the dates of an unfocused month.
        calendar.setUnfocusedMonthDateColor(getResources().getColor(R.color.transparent));

        //sets the color for the separator line between weeks.
        calendar.setWeekSeparatorLineColor(getResources().getColor(R.color.transparent));

        //sets the color for the vertical bar shown at the beginning and at the end of the selected date.
        calendar.setSelectedDateVerticalBar(R.color.red_btn_hover);

        //sets the listener to be notified upon selected date change.
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            //show the selected date as a toast
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int day) {
//                calTv.setText("Departure Date : " + day + "/" + (month + 1) + "/" + year);
            }
        });
    }


}


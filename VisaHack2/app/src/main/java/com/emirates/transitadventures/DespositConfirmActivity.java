package com.emirates.transitadventures;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Main class hosting the navigation drawer
 *
 * @author Sotti https://plus.google.com/+PabloCostaTirado/about
 */
public class DespositConfirmActivity extends AppCompatActivity {


    TextView mCurrentAmount, mAmountPay, mNewAmount;

    ImageView mImageCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit_confirm);

        initialise();
    }

    /**
     * Bind, create and set up the resources
     */
    private void initialise() {
        // Toolbar
        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("Confirm");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);

        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.anim_enter);

        View Confirm = findViewById(R.id.deposit_cardView);
        Confirm.startAnimation(bottomUp);
        Confirm.setVisibility(View.VISIBLE);

        mImageCard = (ImageView) findViewById(R.id.deposit_confirm_card);
        mImageCard.setImageResource(getIntent().getIntExtra("card_resource", R.drawable.ei_card1));

        mCurrentAmount = (TextView) findViewById(R.id.deposit_confirm_card_balance);
        mAmountPay = (TextView) findViewById(R.id.deposit_confirm_card_amount);
        mNewAmount = (TextView) findViewById(R.id.deposit_confirm_card_new_amount);

        String amountSelected = getIntent().getStringExtra("amountSelected");
        try {
            amountSelected = amountSelected.replaceAll("[^\\d.]", "");
            int amount = Integer.parseInt(amountSelected);
            mAmountPay.setText("Deposit : " + amount + " AED");
            mNewAmount.setText("New Balance : " + (amount + 100) + " AED");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                this.finish();
                return true;
            }
            case R.id.action_home:
                final Intent intent = new Intent(this, NavigationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(LoginActivity.VAR_MEMBER_LOGIN, true);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void onHomeClick(View view) {
        final Intent intent = new Intent(this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(LoginActivity.VAR_MEMBER_LOGIN, true);
        startActivity(intent);

    }

    public void onConfirmClick(View view) {
        android.support.v7.app.AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage("Your transaction is successful,Your reference number is #84379870909483");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,  "Back to Home",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        final Intent intent = new Intent(DespositConfirmActivity.this, NavigationActivity.class);
                        intent.putExtra(LoginActivity.VAR_MEMBER_LOGIN, true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                });
        alertDialog.show();
    }


}

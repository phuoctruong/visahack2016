package com.emirates.transitadventures.model;

import java.io.Serializable;

/**
 * Created by s728304 on 28/08/2015.
 */
public class RowItem implements Serializable{

    private int imageId;
    private String fileName;
    private String name;
    private String details;
    private String timings;
    private String price;
    private String type;

    public RowItem(int imageId, String fileName) {
        this.imageId = imageId;
        this.fileName = fileName;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getTimings() {
        return timings;
    }

    public void setTimings(String timings) {
        this.timings = timings;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

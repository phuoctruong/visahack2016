package com.emirates.transitadventures;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PaymentFragment extends AppCompatActivity implements View.OnClickListener {
    private TextView tv_contact_us, tv_credit_card, tv_email_us;
    private LinearLayout ll_creditcard, ll_email;
    private boolean isCCshown, isEmailShown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_payment);
        tv_contact_us = (TextView) findViewById(R.id.tv_contact_us);
        tv_credit_card = (TextView) findViewById(R.id.tv_cc);
        tv_email_us = (TextView) findViewById(R.id.email_us);
        ll_creditcard = (LinearLayout) findViewById(R.id.ll_cc);
        ll_email = (LinearLayout) findViewById(R.id.ll_email);
        tv_credit_card.setOnClickListener(this);
        tv_email_us.setOnClickListener(this);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#0E86C9")));

    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_cc) {
            if (!isCCshown) {
                isCCshown = true;
                ll_creditcard.setVisibility(View.VISIBLE);
            } else {
                ll_creditcard.setVisibility(View.GONE);
                isCCshown = false;
            }
        } else if (v.getId() == R.id.email_us) {
            if (!isEmailShown) {
                isEmailShown = true;
                ll_email.setVisibility(View.VISIBLE);
            } else {
                ll_email.setVisibility(View.GONE);
                isEmailShown = false;
            }
        }
    }
}


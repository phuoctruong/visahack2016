package com.emirates.transitadventures;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by s728304 on 14/11/2015.
 */
public class QRScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    private String amountSelected;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(R.layout.activity_scan_qr);                // Set the scanner view as the content view
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.contentFrmae);
        frameLayout.addView(mScannerView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("Scan Your Code");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);

    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                this.finish();
                return true;
            }
            case R.id.action_home:
                final Intent intent = new Intent(this, NavigationActivity.class);
                intent.putExtra(LoginActivity.VAR_MEMBER_LOGIN, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void handleResult(Result result) {
        //mScannerView.resumeCameraPreview(this);

        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.anim_enter);
        findViewById(R.id.cash_loaded_frame).setVisibility(View.VISIBLE);
        findViewById(R.id.contentFrmae).setVisibility(View.GONE);

        View card1 = findViewById(R.id.cash_loaded_frame_card_view);
        card1.setVisibility(View.VISIBLE);
        TextView textView = (TextView) findViewById(R.id.cash_loaded_text_amount);
        amountSelected = result.getText();
        textView.setText(amountSelected + " AED Credited");
        card1.startAnimation(bottomUp);

        getSupportActionBar().setTitle("Success");
    }

    public void onConfirmClick(View view) {
        final Intent intent1 = new Intent(this, SelectCardActivity.class);
        intent1.putExtra("amountSelected", amountSelected);
        startActivity(intent1);
        overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
        this.finish();
    }

    public void onHomeClick(View view) {
        final Intent intent = new Intent(this, NavigationActivity.class);
        intent.putExtra(LoginActivity.VAR_MEMBER_LOGIN, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

}

package com.emirates.transitadventures;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

/**
 * Created by s728304 on 14/11/2015.
 */
public class SplashActivity extends Activity {

    private Handler handler;
    private TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);
        mTitle = (TextView) findViewById(R.id.splash_id);
        mTitle.setText("paySmart");
        if (getActionBar() != null) {
            getActionBar().hide();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
                finish();
            }
        }, 1000);
    }
}

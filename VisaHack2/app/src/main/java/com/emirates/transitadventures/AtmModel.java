package com.emirates.transitadventures;

public class AtmModel {
    public static final int STANDARD_CHARTER = 0;
    public static final int FIRST_GULF_BANK = 1;
    public static final int EMIRATES_NBD = 2;
    public String photo;
    public String branch;
    public String lastUsed;
    public String location;
    public int bank_type;
}

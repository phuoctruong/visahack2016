package com.emirates.transitadventures;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;


/**
 * Main class hosting the navigation drawer
 *
 * @author Sotti https://plus.google.com/+PabloCostaTirado/about
 */
public class SelectAmountActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atm_select_amount);

        initialise();
    }

    /**
     * Bind, create and set up the resources
     */
    private void initialise() {
        // Toolbar
        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("Select Amount");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);

        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.anim_enter);

        View AED500 = findViewById(R.id.money_card_500);
        AED500.setTag("500");
        AED500.setOnClickListener(this);
        AED500.startAnimation(bottomUp);
        AED500.setVisibility(View.VISIBLE);


        View AED1000 = findViewById(R.id.money_card_1000);
        AED1000.setTag("1000");
        AED1000.setOnClickListener(this);
        AED1000.startAnimation(bottomUp);
        AED1000.setVisibility(View.VISIBLE);

        View AED2000 = findViewById(R.id.money_card_2000);
        AED2000.setTag("2000");
        AED2000.setOnClickListener(this);
        AED2000.startAnimation(bottomUp);
        AED2000.setVisibility(View.VISIBLE);

        View AED3000 = findViewById(R.id.money_card_3000);
        AED3000.setTag("3000");
        AED3000.setOnClickListener(this);
        AED3000.startAnimation(bottomUp);
        AED3000.setVisibility(View.VISIBLE);

        View AEDOther = findViewById(R.id.money_card_other);
        AEDOther.setOnClickListener(this);
        AEDOther.startAnimation(bottomUp);
        AEDOther.setVisibility(View.VISIBLE);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                this.finish();
                return true;
            }
            case R.id.action_home:
                final Intent intent = new Intent(this, NavigationActivity.class);
                intent.putExtra(LoginActivity.VAR_MEMBER_LOGIN, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {

        final Intent intent = new Intent(this, ComfirmActivity.class);
        if (view.getTag() != null) {
            intent.putExtra("amount", view.getTag().toString());
        }
        startActivity(intent);
        overridePendingTransition(R.anim.abc_slide_in_bottom, 0);

    }
}

package com.emirates.transitadventures.piechart;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.emirates.transitadventures.R;
import com.emirates.transitadventures.model.RowItem;

import java.util.ArrayList;

/**
 * Created by S442095 on 28/08/2015.
 */
public class PieChart extends View {

    public interface OnSelectedLisenter {
        public abstract void onSelected(RowItem rowItem, PieChart obj);
    }

    private static String[] PIE_COLORS = null;
    private static int iColorListSize = 0;
    private OnSelectedLisenter onSelectedListener = null;
    private static final String TAG = PieChart.class.getName();
    public static final String ERROR_NOT_EQUAL_TO_100 = "NOT_EQUAL_TO_100";
    private static final int DEGREE_360 = 360;
    private Paint paintPieFill;
    private Paint paintPieBorder;
    private ArrayList<Float> alPercentage = new ArrayList<Float>();
    private int iDisplayWidth, iDisplayHeight;
    private int iSelectedIndex = -1;
    private int iCenterWidth = 0;
    // private int iMargin = 0;
    private int iDataSize = 0;
    private RectF r = null;
    private float fDensity = 0.0f;
    private float fStartAngle = 0.0f;
    private float fEndAngle = 0.0f;

    ArrayList<RowItem> mRowItems;

    int Width = 0;

    public PieChart(Context context, int width, ArrayList<RowItem> items) {
        super(context);
        this.Width = width;
        fnGetDisplayMetrics(context);
        PIE_COLORS = getResources().getStringArray(R.array.colors);
        iColorListSize = PIE_COLORS.length;
        //  iMargin = (int) fnGetRealPxFromDp(5);
        // used for paint circle
        paintPieFill = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintPieFill.setStyle(Paint.Style.FILL);
        // used for paint border
        paintPieBorder = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintPieBorder.setStyle(Paint.Style.STROKE);
        paintPieBorder.setStrokeWidth(fnGetRealPxFromDp(5));
        paintPieBorder.setColor(Color.GRAY);

        mRowItems = items;
    }

    // set listener
    public void setOnSelectedListener(OnSelectedLisenter listener) {
        this.onSelectedListener = listener;
    }

    private boolean isDone = true;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (int i = 0; i < iDataSize; i++) {
            if (i >= iColorListSize) {
                paintPieFill.setColor(Color.parseColor(PIE_COLORS[i % iColorListSize]));
            } else {
                paintPieFill.setColor(Color.parseColor(PIE_COLORS[i]));
            }
            fEndAngle = alPercentage.get(i);
            fEndAngle = fEndAngle / 100 * DEGREE_360;
            canvas.drawArc(r, fStartAngle, fEndAngle, true, paintPieFill);

            Bitmap bmp1 = Bitmap.createBitmap(Width, Width, Bitmap.Config.ARGB_8888);
            Bitmap bmp2 = bmp1.copy(Bitmap.Config.ARGB_8888, true);
            Canvas canvas1 = new Canvas(bmp2);
            canvas1.drawColor(0, PorterDuff.Mode.CLEAR);
            canvas1.drawArc(r, fStartAngle, fEndAngle, true, paintPieFill);

            Bitmap original = BitmapFactory.decodeResource(getResources(), mRowItems.get(i).getImageId());
            original = Bitmap.createScaledBitmap(original, Width, Width, true);
            Bitmap mask = bmp2;
            Bitmap result = Bitmap.createBitmap(Width, Width, Bitmap.Config.ARGB_8888);
            Canvas mCanvas = new Canvas(result);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            mCanvas.drawBitmap(original, 0, 0, null);
            mCanvas.drawBitmap(mask, 0, 0, paint);
            paint.setXfermode(null);
            canvas.drawBitmap(result, 0, r.top, paintPieFill);
            fStartAngle = fStartAngle + fEndAngle;
        }

        canvas.drawCircle(Width / 2, Width / 2, (Width / 2) - 5, paintPieBorder);
    }

    public Bitmap bitmap;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        iDisplayWidth = MeasureSpec.getSize(widthMeasureSpec);
        iDisplayHeight = MeasureSpec.getSize(heightMeasureSpec);
        if (iDisplayWidth > iDisplayHeight) {
            iDisplayWidth = iDisplayHeight;
        }
        iCenterWidth = iDisplayWidth / 2;
        int iR = iCenterWidth;
        if (r == null) {
            r = new RectF(iCenterWidth - iR,  // top
                    iCenterWidth - iR,        // left
                    iCenterWidth + iR,        // rights
                    iCenterWidth + iR);       // bottom
        }
        setMeasuredDimension(iDisplayWidth, iDisplayWidth);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // get degree of the touch point
        double dx = Math.atan2(event.getY() - iCenterWidth, event.getX() - iCenterWidth);
        float fDegree = (float) (dx / (2 * Math.PI) * DEGREE_360);
        fDegree = (fDegree + DEGREE_360) % DEGREE_360;
        // get the percent of the selected degree
        float fSelectedPercent = fDegree * 100 / DEGREE_360;
        // check which pie was selected
        float fTotalPercent = 0;
        for (int i = 0; i < iDataSize; i++) {
            fTotalPercent += alPercentage.get(i);
            if (fTotalPercent > fSelectedPercent) {
                iSelectedIndex = i;
                break;
            }
        }
        if (onSelectedListener != null) {
            onSelectedListener.onSelected(mRowItems.get(iSelectedIndex), this);
        }
        invalidate();
        return super.onTouchEvent(event);
    }

    private void fnGetDisplayMetrics(Context cxt) {
        final DisplayMetrics dm = cxt.getResources().getDisplayMetrics();
        fDensity = dm.density;
    }

    private float fnGetRealPxFromDp(float fDp) {
        return (fDensity != 1.0f) ? fDensity * fDp : fDp;
    }

    public void setAdapter(ArrayList<Float> alPercentage) throws Exception {
        this.alPercentage = alPercentage;
        iDataSize = alPercentage.size();
        float fSum = 0;
        for (int i = 0; i < iDataSize; i++) {
            fSum += alPercentage.get(i);
        }
        if (fSum != 100) {
            Log.e(TAG, ERROR_NOT_EQUAL_TO_100);
            iDataSize = 0;
            throw new Exception(ERROR_NOT_EQUAL_TO_100);
        }
    }

}
package com.emirates.transitadventures;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ApplyPackageActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.apply_view);

        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);

        getSupportActionBar().setTitle("Apply Package");
        TextView description = (TextView) findViewById(R.id.descriptionId);
        Button apply = (Button) findViewById(R.id.apply_now_btn);
        apply.setOnClickListener(this);
        description.setText(Html.fromHtml("<b>Emirates Islamic offers a series of Shari\'a compliant credit cards that enable you to manage your finances effectively</b><br/>With worldwide acceptance at millions of outlets, our cards offer a host of features and benefits that make your life simpler and more convenient."
                + "<br><b>Auto Finance is designed to make buying your car simple and completely hassle free, giving you the opportunity to really enjoy your purchase</b><br/>"
                + "<br><b>Hyat Superior facilitates you in designing the best life for your family by protecting their dreams against all uncertainties and building a pool of savings for all important goals of life.</b>Hyat Superior facilitates you in designing the best life for your family by protecting their dreams against all uncertainties and building a pool of savings for all important goals of life." +
                "<br>With this plan your family remains protected from any financial loss while you enjoy the benefits of regular savings and wealth creation; all in a safe Sharia'h compliant way"));


    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent(this, FetchContactActivity.class);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                this.finish();
                return true;
            }
            case R.id.action_home:
                final Intent intent = new Intent(this, NavigationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


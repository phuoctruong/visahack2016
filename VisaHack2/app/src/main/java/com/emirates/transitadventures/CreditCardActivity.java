package com.emirates.transitadventures;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.emirates.transitadventures.piechart.PieChatActivity;

/**
 * Created by S441634 on 29/08/2015.
 */
public class CreditCardActivity extends AppCompatActivity {
    private Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credit_card);
        submit = (Button) findViewById(R.id.submit_button_login);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), PieChatActivity.class));
            }
        });
    }
}

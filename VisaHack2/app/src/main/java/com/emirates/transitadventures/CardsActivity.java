package com.emirates.transitadventures;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.emirates.transitadventures.piechart.PieChatActivity;

/**
 * Created by s728304 on 29/08/2015.
 */
public class CardsActivity extends AppCompatActivity {

    Fragment mFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_card);
        if(getActionBar() != null){
            getActionBar().setTitle(R.string.nav_drawer_item_app_settings);

        }

    }

    public void buildPackage(View view) {

        Intent intent = new Intent(this, PieChatActivity.class);
        startActivity(intent);
    }

    public void resetListAdapter(View view) {

        final android.support.v4.app.Fragment fragment = getSupportFragmentManager().findFragmentByTag("cards_fragment");
        if (fragment != null && fragment instanceof CardsFragment) {
            ((CardsFragment) fragment).refresh();
        }


    }
}

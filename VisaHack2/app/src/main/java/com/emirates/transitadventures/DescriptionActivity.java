package com.emirates.transitadventures;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;

import com.emirates.transitadventures.model.RowItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by s728304 on 28/08/2015.
 */
public class DescriptionActivity extends AppCompatActivity {

    private RowItem mRowItem;
    AutoCompleteTextView mTagCashTextView;
    TagCashAdapter mTagCashAdapter;
    List<String> mTagProductList;
    List<String> mTagCardList;
    Button mBuyButton;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.item_details);

        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);

        mRowItem = (RowItem) getIntent().getSerializableExtra("rowITem");
        getSupportActionBar().setTitle("Costa Coffee");

        ImageView image = (ImageView) findViewById(R.id.image);
        image.setImageResource(mRowItem.getImageId());

        mTagCashTextView = (AutoCompleteTextView) findViewById(R.id.tag_cash_textview);
        mTagCashAdapter = new TagCashAdapter(this, new ArrayList<String>());
        mTagCashTextView.setAdapter(mTagCashAdapter);
        mTagCashTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                doTextChanged(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //init values
        mTagProductList = new ArrayList<>();
        mTagProductList.add("Big Mac 20 AED");
        mTagProductList.add("Big Tasty 25 AED");
        mTagProductList.add("Whooper 23 AED");
        mTagProductList.add("Chicken 15 AED");
        mTagProductList.add("Salad 10 AED");
        mTagProductList.add("Pepsi 5 AED");
//        mTagProductList.add("Nike New Balance 200 AED");
//        mTagProductList.add("Nike Air Max AED");
        mTagCashAdapter.putTagData(0, mTagProductList);

        mTagCardList = new ArrayList<>();
        mTagCardList.add("Card 1 - 4111");
        mTagCardList.add("Card 2 - 5777");
        mTagCardList.add("Card 3 - 1234");
        mTagCashAdapter.putTagData(1, mTagCardList);

        mBuyButton = (Button) findViewById(R.id.book_season_btn);
        mBuyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doBuyNow();
            }
        });
    }

    private void doTextChanged (CharSequence sequence){
        String[] strings = sequence.toString().split("#");
        if(strings.length == 3){
            String product = strings[1];
            String card = strings[2];
            boolean foundPro = false;
            boolean foundCard = false;
            for(String pro : mTagProductList){
                if(product.equalsIgnoreCase(pro)){
                    foundPro = true;
                    break;
                }
            }
            for(String ca : mTagCardList){
                if(card.equalsIgnoreCase(ca)){
                    foundCard = true;
                    break;
                }
            }
            if(foundPro && foundCard){
                mBuyButton.setVisibility(View.VISIBLE);
                hideKeyBoard();
            } else {
                mBuyButton.setVisibility(View.GONE);
            }
        }
    }

    private void hideKeyBoard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void doBuyNow(){
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, "Please Wait...");
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
                showTransactionSuccess();
            }
        }, 1000);
    }

    private void showTransactionSuccess(){
        android.support.v7.app.AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage("Your transaction is successful,Your reference number is #897492789270");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Back to Home",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        final Intent intent = new Intent(DescriptionActivity.this, NavigationActivity.class);
                        intent.putExtra(LoginActivity.VAR_MEMBER_LOGIN, true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                });
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                this.finish();
                return true;
            }
            case R.id.action_home:
                final Intent intent = new Intent(this, NavigationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

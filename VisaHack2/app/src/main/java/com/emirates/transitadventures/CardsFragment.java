package com.emirates.transitadventures;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.emirates.transitadventures.model.RowItem;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class CardsFragment extends Fragment {

    private ArrayAdapter arrayAdapter;
    private List<RowItem> rowItems;
    private RowItem currentRowItem = null;
    private List<RowItem> likedItems = new ArrayList<>();
    private JSONArray mItemArray;
    private TextView mCardName;

    @InjectView(R.id.frame)
    SwipeFlingAdapterView flingContainer;

    public void refresh() {

        if(arrayAdapter != null){
            arrayAdapter.clear();
            arrayAdapter.addAll(getListItems());
            arrayAdapter.notifyDataSetChanged();


        }
    }


    private List<RowItem> getListItems(){
        final List<RowItem> rowItems = new ArrayList<RowItem>();

        int resID=0;
        int imgnum=0;

        do {
            String fileName = "act_"+imgnum;
            resID=getResources().getIdentifier(fileName, "drawable", "com.emirates.transitadventures");
            if (resID!=0) {
                RowItem rowItem = getItemDescription(fileName, resID);
                rowItems.add(rowItem);
            }
            imgnum++;
        }while (resID!=0);


        return rowItems;
    };

    private RowItem getItemDescription(final String fileName, final int imageId){
        RowItem rowItem = null;
        if(mItemArray != null){
            for(int i=0; i<mItemArray.length(); i++){
                try{
                    final JSONObject jsonData = mItemArray.getJSONObject(i);
                    final String itemFileName = jsonData.getString("file_name");
                    if(itemFileName.contains(fileName)){
                        rowItem = new RowItem(imageId, fileName);
                        rowItem.setDetails(jsonData.getString("details"));
                        rowItem.setName(jsonData.getString("name"));
                        rowItem.setPrice(jsonData.getString("price"));
                        rowItem.setTimings(jsonData.getString("timings"));
                        rowItem.setType(jsonData.getString("type"));
                        break;
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }


            }
        }
        return rowItem;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.activity_my, container, false);
        mCardName  =  ((TextView) view.findViewById(R.id.cardName));
        ButterKnife.inject(this, view);

        try{
            final AssetManager assetManager = getContext().getAssets();
            final InputStream inputStream = assetManager.open("AppDescriptions.json", AssetManager.ACCESS_STREAMING);
            byte[] buffer = new byte[1024];

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            while(inputStream.read(buffer) != -1){
                outputStream.write(buffer);
            }
            inputStream.close();
            outputStream.flush();
            final String jsonString = new String(outputStream.toByteArray(), "UTF-8");
            outputStream.close();
            final JSONObject jsonObject = new JSONObject(jsonString);
            final JSONObject appDesc = (JSONObject)jsonObject.get("AppDescriptions");
            mItemArray = appDesc.getJSONArray("item");

        }catch (Exception ex){
            ex.printStackTrace();
        }

        rowItems = getListItems();
        arrayAdapter = new CustomArrayAdapter(getContext(), R.layout.item, rowItems );
        flingContainer.setAdapter(arrayAdapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                // this is the simplest way to delete an object from the Adapter (/AdapterView)
                Log.d("LIST", "removed object!");

                rowItems.remove(0);

                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                final RowItem rowItem = (RowItem) dataObject;
//                mCardName.setText(rowItem.getName());
                if(rowItem.getType() != null && rowItem.getType().trim().length() > 0){
                    makeToast(getContext(), rowItem.getDetails());
                }


            }


            @Override
            public void onRightCardExit(Object dataObject) {
                final RowItem rowItem = (RowItem) dataObject;
//                mCardName.setText(rowItem.getName());
                if(rowItem.getType() != null && rowItem.getType().trim().length() > 0){
                    makeToast(getContext(), rowItem.getDetails());
                }

//                likedItems.add((RowItem)dataObject);
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
//                RowItem rowItem = new RowItem(R.drawable.shopping_1_dubai_mall, "Dubai Mall", "Dubai Mall Desc");
//                rowItems.add(rowItem);
//
//                rowItem = new RowItem(R.drawable.sightseeing_1_burjalarab, "Burj AlArab", "Burj AlArab");
//                rowItems.add(rowItem);
//                arrayAdapter.notifyDataSetChanged();
//                Log.d("LIST", "notified");
//                i++;

//                arrayAdapter.clear();
//                arrayAdapter.addAll(getListItems());
                if(itemsInAdapter == 1){
                    arrayAdapter.clear();
                    arrayAdapter.addAll(getListItems());
                    arrayAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }
        });


        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {

                final RowItem rowItem = (RowItem)dataObject;
                if(rowItem.getType() == null || rowItem.getType().trim().length() == 0){
                    final Intent intent = new Intent(getActivity(), DescriptionActivity.class);
                    intent.putExtra("rowITem", rowItem);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
                }

            }
        });

        return view;
    }





    static void makeToast(Context ctx, String s){
//        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
    }


    @OnClick(R.id.right)
    public void right() {
        /**
         * Trigger the right event manually.
         */
        flingContainer.getTopCardListener().selectRight();
    }

    @OnClick(R.id.left)
    public void left() {
        flingContainer.getTopCardListener().selectLeft();
    }

    private class CustomArrayAdapter extends ArrayAdapter<RowItem> {


        Context context;

        public CustomArrayAdapter(Context context, int parentResource,
                                  List<RowItem> items) {
            super(context, parentResource, items);
            this.context = context;

        }


        public View getView(int position, View convertView, ViewGroup parent) {
            RowItem rowItem = getItem(position);
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item,parent, false);
            }

            ((ImageView) convertView.findViewById(R.id.image)).setImageResource(rowItem.getImageId());


            return convertView;
        }
    }
}

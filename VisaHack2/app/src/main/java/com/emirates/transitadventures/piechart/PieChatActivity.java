package com.emirates.transitadventures.piechart;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.emirates.transitadventures.ApplyPackageActivity;
import com.emirates.transitadventures.DescriptionActivity;
import com.emirates.transitadventures.NavigationActivity;
import com.emirates.transitadventures.R;
import com.emirates.transitadventures.model.RowItem;

import java.util.ArrayList;

/**
 * Created by S442095 on 28/08/2015.
 */
public class PieChatActivity extends AppCompatActivity implements PieChart.OnSelectedLisenter, View.OnClickListener {

    View[] views = new View[1];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.piechart);

        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, r.getDisplayMetrics());
        width = width - (int) px;

        views[0] = findViewById(R.id.pie_chart1);

        for (int i = 0; i < views.length; i++) {

            FrameLayout frameLayout = (FrameLayout) views[i].findViewById(R.id.pie_chart_frame1);
            ArrayList<RowItem> rowItems = null;
            rowItems = createData1();

            final PieChart pieChart = new PieChart(this, width, rowItems);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width, width);
            frameLayout.addView(pieChart, layoutParams);
            pieChart.setOnSelectedListener(this);
            ArrayList<Float> percentage = new ArrayList<Float>();
            percentage.add(20F);
            percentage.add(20F);
            percentage.add(20F);
            percentage.add(20F);
            percentage.add(20F);
            try {
                pieChart.setAdapter(percentage);
            } catch (Exception e) {
                e.printStackTrace();
            }

            View labelValue = views[i].findViewById(R.id.event1);

            TextView tv = (TextView) labelValue.findViewById(R.id.label);
            tv.setText(rowItems.get(0).getName());
            tv = (TextView) labelValue.findViewById(R.id.time);
            tv.setText(rowItems.get(0).getTimings());

            labelValue = views[i].findViewById(R.id.event2);
            tv = (TextView) labelValue.findViewById(R.id.label);
            tv.setText(rowItems.get(1).getName());
            tv = (TextView) labelValue.findViewById(R.id.time);
            tv.setText(rowItems.get(1).getTimings());

            labelValue = views[i].findViewById(R.id.event3);
            tv = (TextView) labelValue.findViewById(R.id.label);
            tv.setText(rowItems.get(2).getName());
            tv = (TextView) labelValue.findViewById(R.id.time);
            tv.setText(rowItems.get(2).getTimings());

            labelValue = views[i].findViewById(R.id.event4);
            tv = (TextView) labelValue.findViewById(R.id.label);
            tv.setText(rowItems.get(3).getName());
            tv = (TextView) labelValue.findViewById(R.id.time);
            tv.setText(rowItems.get(3).getTimings());

            labelValue = views[i].findViewById(R.id.event5);
            tv = (TextView) labelValue.findViewById(R.id.label);
            tv.setText(rowItems.get(4).getName());
            tv = (TextView) labelValue.findViewById(R.id.time);
            tv.setText(rowItems.get(4).getTimings());
        }

    }


    private ArrayList<RowItem> createData1() {
        ArrayList<RowItem> rowItems = new ArrayList<>();
        addDash(rowItems);
        addDubaiMarina(rowItems);
        addMeusum(rowItems);
        addBurjGhalifa(rowItems);
        addPalm(rowItems);
        return rowItems;
    }

    private void addDash(ArrayList<RowItem> rowItems) {
        RowItem rowItem = new RowItem(R.drawable.act_1, "act_24");
        rowItem.setDetails("Explore this in dubai");
        rowItem.setName("20% offer - McDonald items");
        rowItem.setTimings("");
        rowItem.setPrice("");
        rowItems.add(rowItem);
    }

    private void addDubaiMarina(ArrayList<RowItem> rowItems) {
        RowItem rowItem = new RowItem(R.drawable.act_4, "dubai_marina");
        rowItem.setDetails("The one before the 1 returns in classic colours.");
        rowItem.setName("10% offer - NIKE AIR MAX ZERO");
        rowItem.setTimings("");
        rowItem.setPrice("");
        rowItems.add(rowItem);
    }

    private void addMeusum(ArrayList<RowItem> rowItems) {
        RowItem rowItem = new RowItem(R.drawable.act_2, "meuseum");
        rowItem.setDetails("Enjoy the discounts till 31st of Dec.");
        rowItem.setName("20% off on Costa Coffee and latte");
        rowItem.setTimings("");
        rowItem.setPrice("");
        rowItems.add(rowItem);
    }

    private void addBurjGhalifa(ArrayList<RowItem> rowItems) {
        RowItem rowItem = new RowItem(R.drawable.act_0, "b_khalifa");
        rowItem.setDetails("");
        rowItem.setName("5% off on return tickets booked in Emirates");
        rowItem.setTimings("");
        rowItem.setPrice("");
        rowItems.add(rowItem);
    }

    private void addPalm(ArrayList<RowItem> rowItems) {
        RowItem rowItem = new RowItem(R.drawable.act_3, "palm");
        rowItem.setDetails("Get ready for the new season");
        rowItem.setName("20% off on Men, Ladies and kids accessories");
        rowItem.setTimings("");
        rowItem.setPrice("");
        rowItems.add(rowItem);
    }


    @Override
    public void onSelected(RowItem rowItem, PieChart pieChart) {
        showDialog(rowItem);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, ApplyPackageActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                this.finish();
                return true;
            }
            case R.id.action_home:
                final Intent intent = new Intent(this, NavigationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialog(RowItem rowItem) {
        final Intent intent = new Intent(this, DescriptionActivity.class);
        intent.putExtra("rowITem", rowItem);
        startActivity(intent);
        overridePendingTransition(R.anim.abc_slide_in_bottom, 0);

    }

    public void onClickBuild(View view) {
        final Intent intent = new Intent(this, ApplyPackageActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
    }
}

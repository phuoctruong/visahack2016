package com.emirates.transitadventures;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class AtmFragment extends Fragment implements View.OnClickListener {


    private static final String EMPTY_STRING = "";
    private static final String CLASS_TYPE_STRING = "Class : ";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_atm_card, null);


        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                R.anim.anim_enter);
        View card1 = view.findViewById(R.id.card_1);
        CardHolder cardHolder1 = new CardHolder(card1, this);
        cardHolder1.imageView.setImageResource(R.drawable.withdraw);
        cardHolder1.textView.setText("Withdraw");
        card1.startAnimation(bottomUp);
        card1.setVisibility(View.VISIBLE);

        View card2 = view.findViewById(R.id.card_3);
        CardHolder cardHolder2 = new CardHolder(card2, this);
        cardHolder2.imageView.setImageResource(R.drawable.deposit);
        cardHolder2.textView.setText("Deposit");
        card2.startAnimation(bottomUp);
        card2.setVisibility(View.VISIBLE);

//        View card2 = view.findViewById(R.id.card_2);
//        CardHolder cardHolder2 = new CardHolder(card2, this);
//        cardHolder2.imageView.setImageResource(R.drawable.deposit);
//        cardHolder2.textView.setText("Deposit");
//        card2.startAnimation(bottomUp);
//        card2.setVisibility(View.VISIBLE);

//        View card3 = view.findViewById(R.id.card_3);
//        CardHolder cardHolder3 = new CardHolder(card3, this);
//        cardHolder3.imageView.setImageResource(R.drawable.paybills);
//        cardHolder3.textView.setText("Pay bills");
//        card3.startAnimation(bottomUp);
//        card3.setVisibility(View.VISIBLE);
//
//        View card4 = view.findViewById(R.id.card_4);
//        CardHolder cardHolder4 = new CardHolder(card4, this);
//        cardHolder4.imageView.setImageResource(R.drawable.trasfer);
//        cardHolder4.textView.setText("Trasfers");
//        card4.startAnimation(bottomUp);
//        card4.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.card_1:
                final Intent intent = new Intent(getActivity(), AtmActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
                break;
            case R.id.card_3:
                final Intent intent1 = new Intent(getActivity(), QRScannerActivity.class);
                startActivity(intent1);
                getActivity().overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
                break;
//            case R.id.card_2:
//                final Intent intent1 = new Intent(getActivity(), QRScannerActivity.class);
//                startActivity(intent1);
//                getActivity().overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
//                break;
//            case R.id.card_3:
//                break;
            case R.id.card_4:
                break;
            default:
                break;
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private class CardHolder {
        public ImageView imageView;
        public TextView textView;

        public CardHolder(View view, View.OnClickListener onClickListener) {
            imageView = (ImageView) view.findViewById(R.id.atm_card_icon);
            textView = (TextView) view.findViewById(R.id.atm_card_text);
            view.setOnClickListener(onClickListener);
        }


    }

}


package com.emirates.transitadventures;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.View;

/**
 * Created by s728304 on 14/11/2015.
 */
public class BeamActivity extends Activity {

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beam_layout);
        if (getActionBar() != null) {
            getActionBar().hide();
        }
    }


    public void onSuccesClick(View view) {

        android.support.v7.app.AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage("Your transaction is successful,Your reference number is #897492789270");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Back to Home",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        final Intent intent = new Intent(BeamActivity.this, NavigationActivity.class);
                        intent.putExtra(LoginActivity.VAR_MEMBER_LOGIN, true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                });
        alertDialog.show();

    }

    @Override
    public void onBackPressed() {

    }
}

package com.emirates.transitadventures;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * Main class hosting the navigation drawer
 *
 * @author Sotti https://plus.google.com/+PabloCostaTirado/about
 */
public class SelectCardActivity extends AppCompatActivity implements View.OnClickListener {

    TextView mMasterCardText, mVisaCardText;
    ImageView mMasterCardIC, mVisaCardIC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atm_card_list);

        initialise();
    }

    /**
     * Bind, create and set up the resources
     */
    private void initialise() {
        // Toolbar
        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("Select Card");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);

        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.anim_enter);

        View card1 = findViewById(R.id.master_card);
        card1.setOnClickListener(this);
        mMasterCardText = (TextView) findViewById(R.id.master_card_limit);
        mMasterCardIC = (ImageView) findViewById(R.id.master_card_icon);
        card1.startAnimation(bottomUp);
        card1.setVisibility(View.VISIBLE);
        card1.setTag(R.drawable.ei_card1);

        View card2 = findViewById(R.id.visa_card);
        mVisaCardText = (TextView) findViewById(R.id.visa_card_text);
        mVisaCardIC = (ImageView) findViewById(R.id.visa_card_icon);
        card2.setOnClickListener(this);
        card2.startAnimation(bottomUp);
        card2.setVisibility(View.VISIBLE);
        card2.setTag(R.drawable.ei_card2);

        String amountSelected = getIntent().getStringExtra("amountSelected");
        if (amountSelected != null) {
            mVisaCardText.setText("Statement Balance : " + amountSelected + " AED");
            mMasterCardText.setText("Balance : 100 AED");
        }
        new HttpRequest().execute();
    }

    private class HttpRequest extends AsyncTask<Void, Void, Void> {
        String result = "fail";

        @Override
        protected Void doInBackground(Void... params) {
            result = getVisaResponse();
            return null;
        }

        final String getVisaResponse() {
            String url = "https://sandbox.api.visa.com/vmorc/offers/v1/all";
            BufferedReader inStream = null;
            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpRequest = new HttpGet(url);
                httpRequest.setHeader("Accept", "application/json");
                httpRequest.setHeader("Authorization", String.valueOf(Base64.encodeBase64("x6sYuOC7HzsEs".getBytes())));

                HttpResponse response = httpClient.execute(httpRequest);
                inStream = new BufferedReader(
                        new InputStreamReader(
                                response.getEntity().getContent()));
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                Log.i("statusCode", statusCode + "");
                StringBuffer buffer = new StringBuffer("");
                String line = "";
                String NL = System.getProperty("line.separator");
                while ((line = inStream.readLine()) != null) {
                    buffer.append(line + NL);
                }
                inStream.close();
                result = buffer.toString();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                if (inStream != null) {
                    try {
                        inStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return result;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                this.finish();
                return true;
            }
            case R.id.action_home:
                final Intent intent = new Intent(this, NavigationActivity.class);
                intent.putExtra(LoginActivity.VAR_MEMBER_LOGIN, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {
        String amountSelected = getIntent().getStringExtra("amountSelected");
        if (amountSelected == null) {
            final Intent intent = new Intent(this, SelectAmountActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
        } else {
            final Intent intent = new Intent(this, DespositConfirmActivity.class);
            intent.putExtra("card_resource", Integer.parseInt(view.getTag().toString()));
            intent.putExtra("amountSelected", amountSelected);
            startActivity(intent);
            overridePendingTransition(R.anim.abc_slide_in_bottom, 0);
        }

    }
}

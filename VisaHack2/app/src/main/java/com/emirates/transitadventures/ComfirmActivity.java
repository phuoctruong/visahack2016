package com.emirates.transitadventures;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;


/**
 * Main class hosting the navigation drawer
 *
 * @author Sotti https://plus.google.com/+PabloCostaTirado/about
 */
public class ComfirmActivity extends AppCompatActivity {


    TextView mAmountTextView;

    EditText mCustomAmount;

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atm_confirmation);

        initialise();
    }

    /**
     * Bind, create and set up the resources
     */
    private void initialise() {
        // Toolbar
        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("Confirm");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);

        if (getIntent().getStringExtra("amount") == null) {

            Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.anim_enter);

            View Confirm = findViewById(R.id.enterAmount_card);
            Confirm.startAnimation(bottomUp);
            Confirm.setVisibility(View.VISIBLE);

            mCustomAmount = (EditText) findViewById(R.id.confirm_amount);
            getSupportActionBar().setTitle("Enter amount");

        } else {
            findViewById(R.id.enterAmount_card).setVisibility(View.GONE);
            showConfirmLayout(getIntent().getStringExtra("amount"));

        }

    }

    private void showConfirmLayout(String amount) {

        getSupportActionBar().setTitle("Confirm Withdrawal");
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.anim_enter);

        View Confirm = findViewById(R.id.confirm_card);
        Confirm.startAnimation(bottomUp);
        Confirm.setVisibility(View.VISIBLE);

        mAmountTextView = (TextView) findViewById(R.id.confirm_amount_requested);
        mAmountTextView.setText(amount);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                this.finish();
                return true;
            }
            case R.id.action_home:
                final Intent intent = new Intent(this, NavigationActivity.class);
                intent.putExtra(LoginActivity.VAR_MEMBER_LOGIN, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onConfirmClick(View view) {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, "Please Wait...");
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
                final Intent intent = new Intent(ComfirmActivity.this, BeamActivity.class);
                startActivity(intent);
            }
        }, 1000);

    }

    public void onHomeClick(View view) {
        final Intent intent = new Intent(this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

    public void onConfirmAmount(View view) {
        Animation moveTotop = AnimationUtils.loadAnimation(this, R.anim.anim_out);
        findViewById(R.id.enterAmount_card).startAnimation(moveTotop);
        showConfirmLayout(mCustomAmount.getText().toString());
    }


}

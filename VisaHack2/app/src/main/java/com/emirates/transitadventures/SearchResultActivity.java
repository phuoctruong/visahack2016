package com.emirates.transitadventures;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchResultActivity extends AppCompatActivity {

    private ScrollView rootScrollLayout;
    private TextView mHeaderSource, mHeaderDestination, mHeaderDate, mDepTime, mArrTime, mNoOfStops, mCurrrency, mCurrencyType, mNoOfPax, mTotalNoHrs, mTotalNoMins;
    private TextView mFlightNo, mOrigin, mDestination, mFlightPeriod, mAircraft, mFlightDuration, mNextDay, mConnectionTimeInterval;
    private LinearLayout mTransitRootLayout, mLLRootLayout, mConnection_panel;
    private View inflatedLlLayout1, inflatedLlLayout2;
    SearchResultModel obj1, obj2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_results_activity);
        rootScrollLayout = (ScrollView) findViewById(R.id.scrollView_list);
        mLLRootLayout = new LinearLayout(this);
        mLLRootLayout.setOrientation(LinearLayout.VERTICAL);
        initData();
        initUI();

    }

    private void initUI() {
        inflatedLlLayout1 = getLayoutInflater().inflate(R.layout.search_result_row, null);

        mHeaderSource = (TextView) inflatedLlLayout1.findViewById(R.id.header_source);
        mHeaderDestination = (TextView) inflatedLlLayout1.findViewById(R.id.header_destination);
        mHeaderDate = (TextView) inflatedLlLayout1.findViewById(R.id.header_date);
        mHeaderDate.setText(obj1.getDate());
        mDepTime = (TextView) inflatedLlLayout1.findViewById(R.id.search_result_item_text_view_departure_time);
        mArrTime = (TextView) inflatedLlLayout1.findViewById(R.id.search_result_item_text_view_destination_time);
        mNoOfStops = (TextView) inflatedLlLayout1.findViewById(R.id.search_result_item_text_view_stop_number);
        mCurrencyType = (TextView) inflatedLlLayout1.findViewById(R.id.search_result_item_text_view_currency);
        mCurrrency = (TextView) inflatedLlLayout1.findViewById(R.id.search_result_item_text_view_price);
        mNoOfPax = (TextView) inflatedLlLayout1.findViewById(R.id.search_result_item_text_view_pax);
        mTotalNoHrs = (TextView) inflatedLlLayout1.findViewById(R.id.search_result_item_text_view_hour);
        mTotalNoMins = (TextView) inflatedLlLayout1.findViewById(R.id.search_result_item_text_view_minute);
        mHeaderSource.setText(obj1.getOrigin());
        mHeaderDestination.setText(obj1.getDestination());
        mDepTime.setText(obj1.getDepTime());
        mArrTime.setText(obj1.getArrTime());
        mCurrencyType.setText(obj1.getCurrencyType());
        mCurrrency.setText(obj1.getCurrency());
        mNoOfStops.setText(obj1.getNoOfStops());
        mNoOfPax.setText(obj1.getNoOfPax());
        mTotalNoHrs.setText(obj1.getDurationHrs());
        mTotalNoMins.setText(obj1.getDurationMins());
        //included layout
        LinearLayout fareFlightDetail1 = (LinearLayout) getLayoutInflater().inflate(R.layout.panel_fare_flight_detail, null);

        mFlightNo = (TextView) fareFlightDetail1.findViewById(R.id.fare_familty_text_view_flight_plane_number);
        mOrigin = (TextView) fareFlightDetail1.findViewById(R.id.fare_familty_text_view_airport_code);
        mFlightPeriod = (TextView) fareFlightDetail1.findViewById(R.id.fare_family_text_view_flight_period);
        mAircraft = (TextView) fareFlightDetail1.findViewById(R.id.fare_family_text_view_airCraft);
        mFlightDuration = (TextView) fareFlightDetail1.findViewById(R.id.fare_family_text_View_flight_duration);
        mNextDay = (TextView) fareFlightDetail1.findViewById(R.id.textView_flightDetails_next_day);
        mConnection_panel = (LinearLayout) fareFlightDetail1.findViewById(R.id.connection_panel);
        mConnection_panel.setVisibility(View.VISIBLE);
        mConnectionTimeInterval = (TextView) fareFlightDetail1.findViewById(R.id.connection_time_interval);
        mTransitRootLayout = (LinearLayout) fareFlightDetail1.findViewById(R.id.transit_layout);
        mTransitRootLayout.setVisibility(View.VISIBLE);

        ArrayList<FlightModel> flightList = obj1.getFlightList();
        mFlightNo.setText(flightList.get(0).getFlightNo());
        mOrigin.setText(flightList.get(0).getTripOrigin() + "-" + flightList.get(0).getTripDestination());
        mFlightPeriod.setText(flightList.get(0).getFlightPerios());
        mAircraft.setText(flightList.get(0).getAirCraftType());
        mFlightDuration.setText(flightList.get(0).getFlightDuration());
        mNextDay.setText(flightList.get(0).getNextDay());
        mConnectionTimeInterval.setText(flightList.get(0).getConnectionTerminal());
        mTransitRootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(SearchResultActivity.this, CardsActivity.class);
                startActivity(intent);
            }
        });
        mConnectionTimeInterval.setVisibility(View.VISIBLE);
        if (flightList.get(0).isTransitPackAvailable())
            mConnectionTimeInterval.setVisibility(View.VISIBLE);

        //included layout
        LinearLayout fareFlightDetail2 = (LinearLayout) getLayoutInflater().inflate(R.layout.panel_fare_flight_detail, null);

        mFlightNo = (TextView) fareFlightDetail2.findViewById(R.id.fare_familty_text_view_flight_plane_number);
        mOrigin = (TextView) fareFlightDetail2.findViewById(R.id.fare_familty_text_view_airport_code);
        mFlightPeriod = (TextView) fareFlightDetail2.findViewById(R.id.fare_family_text_view_flight_period);
        mAircraft = (TextView) fareFlightDetail2.findViewById(R.id.fare_family_text_view_airCraft);
        mFlightDuration = (TextView) fareFlightDetail2.findViewById(R.id.fare_family_text_View_flight_duration);
        mNextDay = (TextView) fareFlightDetail2.findViewById(R.id.textView_flightDetails_next_day);
        mConnectionTimeInterval = (TextView) fareFlightDetail2.findViewById(R.id.connection_time_interval);
        mTransitRootLayout = (LinearLayout) fareFlightDetail2.findViewById(R.id.transit_layout);
        mConnection_panel = (LinearLayout) fareFlightDetail2.findViewById(R.id.connection_panel);
        mConnection_panel.setVisibility(View.GONE);
        mTransitRootLayout.setVisibility(View.GONE);
        mFlightNo.setText(flightList.get(1).getFlightNo());
        mOrigin.setText(flightList.get(1).getTripOrigin() + flightList.get(1).getTripDestination());
        mFlightPeriod.setText(flightList.get(1).getFlightPerios());
        mAircraft.setText(flightList.get(1).getAirCraftType());
        mFlightDuration.setText(flightList.get(1).getFlightDuration());
        mNextDay.setText(flightList.get(1).getNextDay());
        mConnectionTimeInterval.setText(flightList.get(1).getConnectionTerminal());
        mLLRootLayout.addView(inflatedLlLayout1);
        mLLRootLayout.addView(fareFlightDetail1);
        mLLRootLayout.addView(fareFlightDetail2);
        ///////////////////SECOND RECORD
        inflatedLlLayout2 = getLayoutInflater().inflate(R.layout.search_result_row, null);

        mHeaderSource = (TextView) inflatedLlLayout2.findViewById(R.id.header_source);
        mHeaderDestination = (TextView) inflatedLlLayout2.findViewById(R.id.header_destination);
        mHeaderDate = (TextView) inflatedLlLayout2.findViewById(R.id.header_date);
        mHeaderDate.setText(obj2.getDate());
        mDepTime = (TextView) inflatedLlLayout2.findViewById(R.id.search_result_item_text_view_departure_time);
        mArrTime = (TextView) inflatedLlLayout2.findViewById(R.id.search_result_item_text_view_destination_time);
        mNoOfStops = (TextView) inflatedLlLayout2.findViewById(R.id.search_result_item_text_view_stop_number);
        mCurrencyType = (TextView) inflatedLlLayout2.findViewById(R.id.search_result_item_text_view_currency);
        mCurrrency = (TextView) inflatedLlLayout2.findViewById(R.id.search_result_item_text_view_price);
        mNoOfPax = (TextView) inflatedLlLayout2.findViewById(R.id.search_result_item_text_view_pax);
        mTotalNoHrs = (TextView) inflatedLlLayout2.findViewById(R.id.search_result_item_text_view_hour);
        mTotalNoMins = (TextView) inflatedLlLayout2.findViewById(R.id.search_result_item_text_view_minute);
        mHeaderSource.setText(obj2.getOrigin());
        mHeaderDestination.setText(obj2.getDestination());
        mDepTime.setText(obj2.getDepTime());
        mArrTime.setText(obj2.getArrTime());
        mCurrencyType.setText(obj2.getCurrencyType());
        mCurrrency.setText(obj2.getCurrency());
        mNoOfStops.setText(obj2.getNoOfStops());
        mNoOfPax.setText(obj2.getNoOfPax());
        mTotalNoHrs.setText(obj2.getDurationHrs());
        mTotalNoMins.setText(obj2.getDurationMins());
        //included layout
        LinearLayout fareFlightDetai21 = (LinearLayout) getLayoutInflater().inflate(R.layout.panel_fare_flight_detail, null);

        mFlightNo = (TextView) fareFlightDetai21.findViewById(R.id.fare_familty_text_view_flight_plane_number);
        mOrigin = (TextView) fareFlightDetai21.findViewById(R.id.fare_familty_text_view_airport_code);
        mFlightPeriod = (TextView) fareFlightDetai21.findViewById(R.id.fare_family_text_view_flight_period);
        mAircraft = (TextView) fareFlightDetai21.findViewById(R.id.fare_family_text_view_airCraft);
        mFlightDuration = (TextView) fareFlightDetai21.findViewById(R.id.fare_family_text_View_flight_duration);
        mNextDay = (TextView) fareFlightDetai21.findViewById(R.id.textView_flightDetails_next_day);
        mConnection_panel = (LinearLayout) fareFlightDetai21.findViewById(R.id.connection_panel);
        mConnection_panel.setVisibility(View.GONE);
        mConnectionTimeInterval = (TextView) fareFlightDetai21.findViewById(R.id.connection_time_interval);
        mTransitRootLayout = (LinearLayout) fareFlightDetai21.findViewById(R.id.transit_layout);
        mTransitRootLayout.setVisibility(View.GONE);

        ArrayList<FlightModel> flightList21 = obj2.getFlightList();
        mFlightNo.setText(flightList21.get(0).getFlightNo());
        mOrigin.setText(flightList21.get(0).getTripOrigin() + " - " + flightList21.get(0).getTripDestination());
        mFlightPeriod.setText(flightList21.get(0).getFlightPerios());
        mAircraft.setText(flightList21.get(0).getAirCraftType());
        mFlightDuration.setText(flightList21.get(0).getFlightDuration());
        mNextDay.setText(flightList21.get(0).getNextDay());
        mConnectionTimeInterval.setText(flightList21.get(0).getConnectionTerminal());
        mConnectionTimeInterval.setVisibility(View.GONE);

        //included layout
        LinearLayout fareFlightDetail22 = (LinearLayout) getLayoutInflater().inflate(R.layout.panel_fare_flight_detail, null);

        mFlightNo = (TextView) fareFlightDetail22.findViewById(R.id.fare_familty_text_view_flight_plane_number);
        mOrigin = (TextView) fareFlightDetail22.findViewById(R.id.fare_familty_text_view_airport_code);
        mFlightPeriod = (TextView) fareFlightDetail22.findViewById(R.id.fare_family_text_view_flight_period);
        mAircraft = (TextView) fareFlightDetail22.findViewById(R.id.fare_family_text_view_airCraft);
        mFlightDuration = (TextView) fareFlightDetail22.findViewById(R.id.fare_family_text_View_flight_duration);
        mNextDay = (TextView) fareFlightDetail22.findViewById(R.id.textView_flightDetails_next_day);
        mConnectionTimeInterval = (TextView) fareFlightDetail22.findViewById(R.id.connection_time_interval);
        mTransitRootLayout = (LinearLayout) fareFlightDetail22.findViewById(R.id.transit_layout);
        mConnection_panel = (LinearLayout) fareFlightDetail22.findViewById(R.id.connection_panel);
        mConnection_panel.setVisibility(View.GONE);
        mTransitRootLayout.setVisibility(View.GONE);
        mFlightNo.setText(flightList21.get(1).getFlightNo());
        mOrigin.setText(flightList21.get(1).getTripOrigin() + flightList21.get(1).getTripDestination());
        mFlightPeriod.setText(flightList21.get(1).getFlightPerios());
        mAircraft.setText(flightList21.get(1).getAirCraftType());
        mFlightDuration.setText(flightList21.get(1).getFlightDuration());
        mNextDay.setText(flightList21.get(1).getNextDay());
        mConnectionTimeInterval.setText(flightList21.get(1).getConnectionTerminal());
        mLLRootLayout.addView(inflatedLlLayout2);
        mLLRootLayout.addView(fareFlightDetai21);
        mLLRootLayout.addView(fareFlightDetail22);

        rootScrollLayout.addView(mLLRootLayout);

    }

    private void initData() {
        //first record
        obj1 = new SearchResultModel();
        obj1.setOrigin("LHR");
        obj1.setDestination("SYD");
        obj1.setDate("Wednesday 21 Oct");
        obj1.setDepTime("09:05");
        obj1.setArrTime("08:45");
        obj1.setNoOfStops("1");
        obj1.setCurrencyType("GBP");
        obj1.setCurrency("3,523.04");
        obj1.setNoOfPax("1 Passenger");
        obj1.setDurationHrs("37 hrs");
        obj1.setDurationMins("40 mins");

        ArrayList<FlightModel> flightList = new ArrayList<FlightModel>();

        FlightModel flightObj = new FlightModel();
        flightObj.setFlightNo("EK008");
        flightObj.setTripOrigin("LHR");
        flightObj.setTripDestination("DXB");
        flightObj.setFlightPerios("09:05 - 19:05");
        flightObj.setAirCraftType("A380-800");
        flightObj.setFlightDuration("Duration : 7 hrs");
        flightObj.setConnectionTerminal("Connection 13hrs 45mins");
        flightObj.setIsTransitPackAvailable(true);
        flightList.add(flightObj);

        FlightModel flightObj1 = new FlightModel();
        flightObj1.setFlightNo("EK418");
        flightObj1.setTripOrigin("DXB");
        flightObj1.setTripDestination("-BKK-SYD");
        flightObj1.setFlightPerios("08:50 - 08:45");
        flightObj1.setAirCraftType("777-300ER");
        flightObj1.setFlightDuration("Duration : 16hrs 55mins");
        flightObj1.setIsTransitPackAvailable(false);
        flightList.add(flightObj1);

        obj1.setFlightList(flightList);

        //second record

        //first record
        obj2 = new SearchResultModel();
        obj2.setOrigin("LHR");
        obj2.setDestination("SYD");
        obj2.setDate("Wednesday 21 Oct");
        obj2.setDepTime("22:15");
        obj2.setArrTime("07:00");
        obj2.setNoOfStops("1");
        obj2.setCurrencyType("GBP");
        obj2.setCurrency("3,523.04");
        obj2.setNoOfPax("1 Passenger");
        obj2.setDurationHrs("22 hrs");
        obj2.setDurationMins("45 mins");

        ArrayList<FlightModel> flightList2 = new ArrayList<FlightModel>();

        FlightModel flightList20 = new FlightModel();
        flightList20.setFlightNo("EK006");
        flightList20.setTripOrigin("LHR");
        flightList20.setTripDestination("DXB");
        flightList20.setFlightPerios("22:15 - 08:05");
        flightList20.setAirCraftType("A380-800");
        flightList20.setFlightDuration("Duration : 6hrs 50mins");
        flightList20.setConnectionTerminal("Connection 2hrs 45mins");
        flightList20.setIsTransitPackAvailable(false);
        flightList2.add(flightList20);

        FlightModel flightList21 = new FlightModel();
        flightList21.setFlightNo("EK412");
        flightList21.setTripOrigin("DXB");
        flightList21.setTripDestination("-SYD");
        flightList21.setFlightPerios("10:15 - 07:00");
        flightList21.setAirCraftType("A380-800ER");
        flightList21.setFlightDuration("Duration : 13hrs 45mins");
        flightList21.setIsTransitPackAvailable(false);
        flightList2.add(flightList21);

        obj2.setFlightList(flightList2);
    }
}

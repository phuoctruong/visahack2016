package com.emirates.transitadventures.customViews;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


/**
 * Created by s729811 on 22/04/2015.
 */
public class NoPaddingTextView extends TextView {
    private int mAdditionalPadding = 0;
    private boolean isCalculating;
    public NoPaddingTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setIncludeFontPadding(false); //remove the font padding
        setGravity(getGravity() | Gravity.TOP); //make sure that the gravity is set to the top
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();
        canvas.translate(0, mAdditionalPadding);
        super.onDraw(canvas);
        canvas.restore();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if(!isCalculating) {
            isCalculating = true;
            mAdditionalPadding = (int) (getTextSize() - getLineHeight());
            ViewGroup.LayoutParams params = getLayoutParams();
            if (params instanceof LinearLayout.LayoutParams) {
                ((LinearLayout.LayoutParams) params).bottomMargin += mAdditionalPadding;
            } else if (params instanceof RelativeLayout.LayoutParams) {
                ((RelativeLayout.LayoutParams) params).bottomMargin += mAdditionalPadding;
            } else if (params instanceof FrameLayout.LayoutParams) {
                ((FrameLayout.LayoutParams) params).bottomMargin += mAdditionalPadding;
            }
        }
        super.onLayout(changed, left, top, right, bottom);

    }
}
